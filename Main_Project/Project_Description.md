I will use image recognition to automate a simple game with Python.
There are two games in mind with 2 different methods.

1. Basically the code will check in every necessary field the color of 1 pixel if it matches the pre given color it will click it.
To do this I will need the RGB value.

2. The second one will actually use the image recognition. In this game all objects look the same. 
With a simple screenshot of the object the code will be able to click it. Every time the object appears on the screen the code will click it.
